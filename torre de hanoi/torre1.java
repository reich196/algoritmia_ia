import java.util.Scanner;

public class torre1 {

    private static void mover(int disco, char origen, char destino) {
        System.out.println("Mover disco " + disco + " de " + origen + " a " + destino);
    }

    public static void hanoi(int n, char origen, char auxiliar, char destino) {
        if (n == 1) {
            mover(1, origen, destino);
            return;
        }
        
        hanoi(n - 1, origen, destino, auxiliar);
        mover(n, origen, destino);
        hanoi(n - 1, auxiliar, origen, destino);
    }

    public static void main(String[] args) {
        int numDiscos;
        Scanner entrada = new Scanner(System.in);

        System.out.print("Ingrese el número de discos: ");
        numDiscos = entrada.nextInt();

        hanoi(numDiscos, 'A', 'B', 'C');
    }
}
